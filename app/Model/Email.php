<?php
App::uses('AppModel', 'Model');
/**
 * EmailQueue Model
 *
 */
class Email extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'correos';

/**
 * Display field
 *
 * @var string
 */
 	public $displayField = 'email';

}
