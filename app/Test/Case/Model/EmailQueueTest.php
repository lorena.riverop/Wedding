<?php
App::uses('EmailQueue', 'Model');

/**
 * EmailQueue Test Case
 *
 */
class EmailQueueTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.email_queue'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EmailQueue = ClassRegistry::init('EmailQueue');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EmailQueue);

		parent::tearDown();
	}

}
