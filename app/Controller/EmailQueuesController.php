<?php
App::uses('AppController', 'Controller');
/**
 * EmailQueues Controller
 *
 * @property EmailQueue $EmailQueue
 * @property PaginatorComponent $Paginator
 */
class EmailQueuesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $uses = array('EmailQueue' , 'Email');

/**
 * index method
 *
 * @return void
 */	
	
	function beforeFilter(){

		$this->layout = 'default2';
	}
	public function index() {
		$this->EmailQueue->recursive = 0;
		$this->set('emailQueues', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EmailQueue->exists($id)) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		$options = array('conditions' => array('EmailQueue.' . $this->EmailQueue->primaryKey => $id));
		$this->set('emailQueue', $this->EmailQueue->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'default2';
		if ($this->request->is('post')) {
			$this->EmailQueue->create();
			if ($this->EmailQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The email queue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email queue could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EmailQueue->exists($id)) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The email queue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email queue could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailQueue.' . $this->EmailQueue->primaryKey => $id));
			$this->request->data = $this->EmailQueue->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EmailQueue->id = $id;
		if (!$this->EmailQueue->exists()) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->EmailQueue->delete()) {
			$this->Session->setFlash(__('The email queue has been deleted.'));
		} else {
			$this->Session->setFlash(__('The email queue could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function verificoToken($token = null){
		$this->autoRender = false;

		if($token){
			$stoken = $this->Email->find('first' , array('conditions' => array('token' => $token)));
			
			if($stoken){
				$this->Session->write('token', 'valido');
				$this->Session->write('token_id' , $token);
				$this->Email->id = $stoken['Email']['id'];
				$this->Email->saveField('abrio', 1);
				header("Location:".$this->webroot);
				exit();
			}
			else {
				header("Location:".$this->webroot);
				exit();
			}	
		}
		else {
			header("Location:".$this->webroot);
			exit();
		}
		
		
		
	}

	public function enviarInvitacion(){
		$this->autoRender = false;
		if($this->request->data['email']){
			

			require 'PHPMailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'mx1.hostinger.com.ar';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'savethedate@weddingnanycarlose.com.ve';                 // SMTP username
			$mail->Password = 'Nenitos17.12.16';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                                    // TCP port to connect to

			$mail->setFrom('boda17.12.16@gmail.com', 'boda17.12.16');
			$mail->addAddress($this->request->data['email'], $this->request->data['nombre']);     // Add a recipient
			$mail->addReplyTo('danielapinzon1990@gmail.com', 'Daniela Pinzon');
		

			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = 'Save the date';
			$mail->Body    = '<a href="http://weddingnanycarlose.com.ve/""><img src="http://weddingnanycarlose.com.ve/img/invitacion.png" /></a>';
		
			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
			    echo 'Message has been sent';
			}
		}


	}

	public function obtenerInvitaciones(){

		$this->autoRender = false;
		$data = array();
		$data = $this->Email->find('all' , array('limit' => 15,'conditions' => array('abrio' => null)));
		$data2 = $this->Email->find('all' , array('limit'=> 15 ,'conditions' => array('asistira_a !=' => null)));
		$data3 = $this->Email->find('all' , array('limit'=> 15 ,'conditions' => array('abrio' => 1)));
		
		$arrayReturn =  array('data' => $data , 'data2' => $data2 , 'data3' => $data3); 
		return json_encode($arrayReturn);
		//var_dump($data[0]);
		//return new CakeResponse(array('body' => json_encode($data)));
	}

	public function confirmoToken(){
		$this->autoRender = false;
		
		
		$asistira_a = $this->request->data['Eventorsvp'];
		$persona = $this->request->data['Nombrersvp'];
		$email = $this->request->data['Correorsvp'];	
		if(mail("danielapinzon1990@gmail.com","confirmacion","La persona ".$persona." , ".$email." y asistira a ".$asistira_a)){
			echo json_encode(1);
		} 

				
		
		
		
	}

	public function enviarCorreos(){
		$this->autoRender = false;
		
		$correo = $this->Email->find('first' ,  array('conditions' => array('is_send' => 1)));
		
		require 'PHPMailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;

			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'boda17.12.16@gmail.com';                 // SMTP username
			$mail->Password = 'Nenitos17.12.16';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                                    // TCP port to connect to

			$mail->setFrom('boda17.12.16@gmail.com', 'boda17.12.16');
			$mail->addAddress($correo['Email']['email']);     // Add a recipient
			$mail->addReplyTo('danielapinzon1990@gmail.com', 'Daniela Pinzon');
		

			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = 'Save the date';
			$mail->Body    = '<a href="http://weddingnanycarlose.com.ve/""><img src="http://weddingnanycarlose.com.ve/img/invitacion.png" /></a>';
		
			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			    $this->Email->id = $correo['Email']['id'];
				$this->Email->saveField('is_send', 0);
			    header("Refresh:10");
			} else {
				$this->Email->id = $correo['Email']['id'];
				$this->Email->saveField('is_send', 0);
				echo 'Message has been sent a '.$correo['Email']['email'];
			    header("Refresh:10");
			    
			}
           
	}

}
