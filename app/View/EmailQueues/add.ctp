<div class="emailQueues form">
<?php echo $this->Form->create('EmailQueue'); ?>
	<fieldset>
		<legend><?php echo __('Add Email Queue'); ?></legend>
	<?php
		echo $this->Form->input('to');
		echo $this->Form->input('from_name');
		echo $this->Form->input('from_email');
		echo $this->Form->input('subject');
		echo $this->Form->input('config');
		echo $this->Form->input('template');
		echo $this->Form->input('layout');
		echo $this->Form->input('format');
		echo $this->Form->input('template_vars');
		echo $this->Form->input('sent');
		echo $this->Form->input('locked');
		echo $this->Form->input('send_tries');
		echo $this->Form->input('send_at');
		echo $this->Form->input('token');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Email Queues'), array('action' => 'index')); ?></li>
	</ul>
</div>
