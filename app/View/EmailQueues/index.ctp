<div class="emailQueues index">
	<h2><?php echo __('Email Queues'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('to'); ?></th>
			<th><?php echo $this->Paginator->sort('from_name'); ?></th>
			<th><?php echo $this->Paginator->sort('from_email'); ?></th>
			<th><?php echo $this->Paginator->sort('subject'); ?></th>
			<th><?php echo $this->Paginator->sort('config'); ?></th>
			<th><?php echo $this->Paginator->sort('template'); ?></th>
			<th><?php echo $this->Paginator->sort('layout'); ?></th>
			<th><?php echo $this->Paginator->sort('format'); ?></th>
			<th><?php echo $this->Paginator->sort('template_vars'); ?></th>
			<th><?php echo $this->Paginator->sort('sent'); ?></th>
			<th><?php echo $this->Paginator->sort('locked'); ?></th>
			<th><?php echo $this->Paginator->sort('send_tries'); ?></th>
			<th><?php echo $this->Paginator->sort('send_at'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('token'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($emailQueues as $emailQueue): ?>
	<tr>
		<td><?php echo h($emailQueue['EmailQueue']['id']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['to']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['from_name']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['from_email']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['subject']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['config']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['template']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['layout']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['format']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['template_vars']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['sent']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['locked']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['send_tries']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['send_at']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['created']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['modified']); ?>&nbsp;</td>
		<td><?php echo h($emailQueue['EmailQueue']['token']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $emailQueue['EmailQueue']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $emailQueue['EmailQueue']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $emailQueue['EmailQueue']['id']), array(), __('Are you sure you want to delete # %s?', $emailQueue['EmailQueue']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Email Queue'), array('action' => 'add')); ?></li>
	</ul>
</div>
