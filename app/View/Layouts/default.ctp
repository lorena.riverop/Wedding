<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Wedding');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<!--
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'> -->
    <link id="page_favicon" href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAycnyAPf3/wDt7f8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAExAAAAAAAAEzMQAAAAAAEzMzEAAAAAEzMzIxAAAAEzMzIiMQAAATMzMiIxAAABMzMTIjEAAAEzMQEzMQAAABEQABEQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA//8AAP7/AAD8fwAA+D8AAPAfAADgDwAAwAcAAMAHAADABwAAwQcAAOOPAAD//wAA//8AAP//AAD//wAA" rel="icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript">
	var webroot="<?php echo $this->webroot;?>";
</script>
	<?php
		//echo $this->Html->meta('icon');
		if($this->Session->read('token') == 'valido'){
			
		}
		else {
			//header("Location: http://example.com/myOtherPage.php");
			//exit();
		}
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('style');
        echo $this->Html->css('animate');
        echo $this->Html->css('loader');

		echo $this->Html->script('jquery');
		echo $this->Html->script('bootstrap');
		echo $this->Html->script('functions');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	   
        
</head>
<body>
	<div id="fountainTextG" class="loader"><div id="fountainTextG_1" class="fountainTextG">D</div><div id="fountainTextG_2" class="fountainTextG"> </div><div id="fountainTextG_3" class="fountainTextG">❤</div><div id="fountainTextG_4" class="fountainTextG"> </div><div id="fountainTextG_5" class="fountainTextG">C</div></div>

	<div id="header" class="scroll invisible">
		
		<div id="textos">
			<div class="SaveTheDate">
				<div id="SaveTheDateText">
					<div id="texto1">Save </div>
					<div id="texto2">the</div>
					<div id="texto3">Date</div>
				</div>
				<div id="texto4"> 
					<h4 class="heartpink">❥❥</h4>
					<h4>17</h4>
					<h6 class="heartpink">❤</h6>
					<h4>12</h4>
					<h6 class="heartpink">❤</h6>
					<h4>2016</h4>
					<h4 class="heartpink" id="corazones">❥❥</h4>
				</div>
			</div>
			<div id="countingdown">
				<div class="margen">
					<div id="dias" class="count-style">
						<p class="numero"></p>
						<p class="textocount">dias</p>	
					</div>
				</div>
				<div class="margen">
					<div id="horas" class="count-style">
						<p class="numero"></p>
						<p class="textocount">horas</p>	
					</div>
				</div>
				<div class="margen">
					<div id="minutos" class="count-style">
						<p class="numero"></p>
						<p class="textocount">minutos</p>	
					</div>
				</div>
				<div class="margen">
					<div id="segundos" class="count-style">
						<p class="numero"></p>
						<p class="textocount">segundos</p>	
					</div>
				</div>
			</div>
		</div>
		 	

	</div><!-- header-->

	
	<div id="container" class="scroll invisible">
		<nav id="menu" class="navbar navbar-default navbar-fixed-top ">
			<div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          
         	</div>
			<div class="container-fluid">
			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="navbar">
			      	<ul class="nav navbar-nav col-xs-12" >
			        	<li><a href="#invitacion">Invitacion</a></li>
			        	<li class="noanimli"><a class="noanim" >❤</a></li>
			        	<li id="confirmacionbutton"><a href="#rsvp">Confirmacion</a></li>
			        	<li class="noanimli"><a class="noanim" >❤</a></li>
			        	<li id="alojamientobutton"><a href="#alojamiento">Alojamiento</a></li>
			        	<li id="HDC"><a href="#"> D <span id="corazon">❤</span> C</a>
			        	</li>
			        	<li><a href="#cortejo">Cortejo</a></li>
			        	<li class="noanimli"><a class="noanim">❤</a></li>
			        	<li><a href="#listaboda">Lista de Boda</a></li>
			        	<li class="noanimli"><a class="noanim">❤</a></li>
			        	<li><a href="#atuendo">Vestimenta</a></li>
			      	</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid ó-->
		</nav>

		<div id="contenido" class="invisible">		
			<div id="invitacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="vacio col-lg-2 col-md-2 hidden-xs hidden-sm"></div>
				<div id="contentinvitacion" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div id="ornamentContainer" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="ornament"></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="noviafam" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div>Gianna Montagna Filippi</div>
							<div>Ruben Pinzón Bernal</div>
						</div>
						<div id="noviofam" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div>Nelly Tortolero Guerra</div>
							<div>Jaime Molina Vargas</div>
						</div>
					</div>
					<div id="textoinv" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="textoinv1">Se complacen en invitarlos al matrimonio de sus  hijos</div>
					</div>
					<div id="NombresDC" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="Novia" class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<div id="Novia1">Daniela</div>
							<div id="Novia2">Alessandra</div>
						</div>
						<div id="Y" class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&</div>
						<div id="Novio" class="medio col-lg-5 col-md-5 col-sm-5 col-xs-5">
							<div id="Novio1">Carlos</div>
							<div id="Novio2">Eduardo</div>
						</div>
					</div>
					<div id="starline"class="medio col-lg-12 col-md-12 col-sm-12 col-xs-12">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . </div>
					
					<div id="textoinv2" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">El sábado diecisiete de Diciembre de 2016 en la Isla de Margarita</div>
					
					<div id= "Lugares"class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="textoinv31" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div class="medio textobold">Ceremonia</div>
							<div>Iglesia del Carmen, El Tirano. Isla de Margarita. 4:00 pm. </div>
							<a href="https://goo.gl/maps/zvghF762tnP2" target="_blank" class="pull-right">Ver mapa</a>
							
						</div>
						<div id="textoinv32" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div class="medio textobold">Recepcion</div>
							<div>IKIN Margarita Hotel & Spa, Calle El Melonal, Sector El Cardón, Isla De Margarita. 5:00 pm.</div>
							<a href="https://goo.gl/maps/ZfMKe6WZWuj" target="_blank" class="pull-right">Ver mapa</a>
						</div>
					</div>
				</div>
				<div class="vacio col-lg-2 col-md-2 hidden-sm hidden-xs"></div>
			</div><!-- Invitacion-->			

			<div id="rsvp" class="col-xs-12 invisible">
				<div id="rsvpmensaje" class="col-lg-7 col-md-7 col-sm-12">
				
					<div id="textomensaje"><span class="mayusculas">Te amo,</span> no sólo por lo que eres, sino por lo que <span class="mayusculas">Soy</span> cuando estoy <span class="mayusculas">Contigo...</span></div>

				</div>
				<div id="rsvpcontenedor" class="col-lg-5 col-md-5 col-sm-12">	
					<div id="textorsvp"  >
						<div id="rsvptitulo">RSVP/Confirmación</div>
						<div id="rsvpcontenido">
							<div class="form-group">
								<label class="opcionrsvp">Nombre:</label>
								<input type="text" class="form-control" id="Nombrersvp">
							</div>
							<div class="form-group">
								<label class="opcionrsvp">Correo electronico:</label>
								<input type="email" class="form-control" id="Correorsvp">
							</div>
							<div class="form-group">
								<label class="opcionrsvp">Evento al que asitiras:</label>
								<select class="form-control" id="Eventorsvp">
									<option >Ceremonia</option>
									<option >Recepcion</option>
									<option >Ambos</option>
								</select>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" id="confirmar">Si, Asistire</button>
							</div>
						</div>
					</div>
				</div>
			</div><!-- RSVP-->

			<div id="alojamiento" class="col-xs-12 invisible">
				<div id="alojamientotitulo" class="titulo col-xs-12">Alojamiento</div>
				<div class="col-xs-12">
					<div class="vacio col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
					<div class="hoteles col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="hotel izquierdo col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="hotelinfo col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<h4>Ikin</h4>
								<p>Calle El Melonal, PB. El Cardón, Edo. Nueva Esparta Isla Margarita, Venezuela T +58 295 500 4555</p>
							</div>
							<div class="thumbnail col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<img src="<?php echo $this->webroot;?>img/ikin1.jpg" >
							</div>
						</div>
						<div class="hotel derecho col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="thumbnail col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<img src="<?php echo $this->webroot;?>img/h20.jpg" >
							</div>
							<div class="hotelinfo col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<h4>h20tel</h4>
								<p>calle Miragua, Playa el Agua - Isla Margarita Reservas: +58 295 249 1684 / 249 0009</p>
							</div>
						</div>
						<div class="hotel izquierdo col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="hotelinfo col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<h4>Villa Maloka</h4>
								<p>Playa Guacuco.Isla de Margarita,Venezuela. 0295-417-4654</p>
							</div>
							<div class="thumbnail col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<img src="<?php echo $this->webroot;?>img/villamaloka.jpg">
							</div>
						</div>
						<div class="hotel derecho col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="thumbnail col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<img src="<?php echo $this->webroot;?>img/sunsol.jpg">
							</div>
							<div class="hotelinfo col-lg-6 -col-md-6 col-sm-12 col-xs-12">
								<h4>Sunsol</h4>
								<p> playa El Tirano, carretera Costanera, 6301, Isla de Margarita 0295-500-50-00</p>
							</div>
						</div>
					</div>
					<div class="vacio col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
				</div>
			</div><!-- ALOJAMIENTO -->

			<div id="cortejo" class="col-xs-12 invisible">
				<div id="cortejotitulo" class="titulo col-xs-12">Cortejo</div>
				<div id="padrinos" class="col-xs-12" >
					<div class="ellstitulo col-xs-12">PADRINOS</div>
					<div id="boxpadrinos" class="col-xs-12">
						<div id="boxmadrina" class="col-sm-6 col-xs-6">
							<div id="madrina">
								<img class ="imgpadrinos" src="<?php echo $this->webroot; ?>img/LorenaRivero.jpg">
								<div class="namecortejo PenelopeAnne">
									<div>Lorena E. Rivero</div>
									<div>Madrina</div>
								</div>
							</div>
						</div>
						<div id="boxpadrino" class="col-sm-6 col-xs-6">
							<div id="padrino">
								<img class ="imgpadrinos" src="<?php echo $this->webroot; ?>img/image1.jpg">
								<div class="namecortejo PenelopeAnne">
									<div>Jaime E. Molina</div>
									<div>Padrino</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- padrinos-->
				<div id="ellas" class="margenimg invisible">
					<div class="ellstitulo">Ellas</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/ConstanzaPinzon.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Constanza Pinzón "Tati"</div>
							<div>Hermana</div>
						</div>
						
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/image2.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Andrea Petkoff</div>
							<div>Amiga</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/ClementinaPinzon.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Clementina Pinzón</div>
							<div>Hermana</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/MariaPinzon.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Maria F. Pinzon</div>
							<div>Prima Hermana</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/MariaRuiz.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Maria G. Ruiz</div>
							<div>Cuñi-amiga</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/VanessaFelice.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Vanessa Felice</div>
							<div>Amiga</div>
						</div>

					</div>
				</div><!-- ellas-->
				<div id="ellos" class="margenimg">
					<div class="ellstitulo">Ellos</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/RicardoMolina.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Ricardo Molina "Pescado"</div>
							<div>Hermano del novio</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/GustavoBerti.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Gustavo Berti Tortolero "Luufy"</div>
							<div>Primo Hermano</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/PabloBerti.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Pablo berti Tortolero "Palito"</div>
							<div>Primo Hermano</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/MiguelMijares.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Miguel Mijares Tortolero "Mickie"</div>
							<div>Primo Hermano</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/image3.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Eduardo Defreitas "Candy"</div>
							<div>Amigo</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/ArturoTortolero.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Arturo Tortolero "Dumita"</div>
							<div>Primo Hermano</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/ellos.svg">
						<div class="namecortejo PenelopeAnne">
							<div>Ivan Martinez</div>
							<div>Amigo</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/DavidAlvitez.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>David Alvitez "Peroano"</div>
							<div>Amigo</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/image4.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Omar Pantoja "Mochilon"</div>
							<div>Amigo</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/RafaelOchoa.jpg">
						<div class="namecortejo PenelopeAnne">
							<div>Rafael Ochoa</div>
							<div>Amigo</div>
						</div>
					</div>
					<div class="cortejocuadro col-sm-6 col-md-4 col-xs-6">
						<img class ="imgcortejo" src="<?php echo $this->webroot; ?>img/ellos.svg">
						<div class="namecortejo PenelopeAnne">
							<div>Luis Fernandez "Pemon</div>
							<div>Amigo</div>
						</div>
					</div>

				</div><!-- ellos-->
				
			</div><!-- Cortejo-->


			<div id="listaboda" class="col-xs-12 invisible">
				<div id="listaBTitulo" class="titulo col-xs-12">Lista de Bodas</div>
				<div id="contentlistaboda" class="col-xs-12"> 
					<div class="vacio col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
					<div id="boxlista" class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="listaitem col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<div class="photolista col-xs-12" >
								<img src="<?php echo $this->webroot; ?>img/iskia.png" width="100%">
							</div>
						</div>
						<div class="listaitem col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<div class="photolista col-xs-12" >
								<img src="<?php echo $this->webroot; ?>img/amazon.jpg" width="100%">
							</div>
						</div>
						<div class="listaitem col-lg-4 col-md-12 col-sm-12 col-xs-12">
							<div class="photolista col-xs-12" >
								<img src="<?php echo $this->webroot; ?>img/12370-NOAIKP.jpg" width="100%">
							</div>
						</div>

					</div>
					<div class="vacio col-lg-1 col-md-1 hidden-sm hidden-xs"></div>
				</div>


			</div><!-- LISTA DE BODA-->

			<div id="atuendo" class="col-xs-12 invisible">
				<div id="atuendoTitulo" class="titulo col-xs-12">Vestimenta</div>
				
				<div id="atuendobox" class="col-xs-12">
					<div id="atuendoEllas" class="col-lg-6 col-md-6 col-sm-12-col-xs-12">
						<div class="medio">
							<img src="<?php echo $this->webroot; ?>img/tacon.svg">
						</div>
						<div id="listaEllas">
							<div id="ellasSi" class=" medio col-sm-6 col-xs-12">
								<div class="SI col-xs-12">God Yes!!!</div>
								<div class="vestlista col-xs-12">
									<div class="itemvest col-xs-12">Vestidos ligeros, vaporosos, sin parecer playeros.</div>
									<div class="itemvest col-xs-12">Colores vivos y tonos claros.</div>
									<div class="itemvest col-xs-12">Pueden llevar tacones (no se pisará la arena en ningún momento).</div>
								</div>
							</div>
							<div id="ellasNo" class=" medio col-sm-6 col-xs-12">
								<div class="NO col-xs-12">Hell No!!!</div>
								<div class="vestlista col-xs-12">
									<div class="itemvest col-xs-12">Color Negro y Blanco.</div>
									<div class="itemvest col-xs-12">Cholitas playeras.</div>
									<div class="itemvest col-xs-12">Vestidos calurosos</div>
									<div class="itemvest col-xs-12">Vestidos Brillantes.</div>
								</div>
							</div>
						</div>
					</div>


					<div id="atuendoEllos" class="col-lg-6 col-md-6 col-sm-12-col-xs-12">
						<div class="medio">
							<img src="<?php echo $this->webroot; ?>img/sombrero.svg">
						</div>
						<div id="listaEllos">
							<div id="ellosSi" class=" medio col-sm-6 col-xs-12">
								<div class="SI col-xs-12">God Yes!!!</div>
								<div class="vestlista col-xs-12">
									<div class="itemvest col-xs-12">Guayaberas de lino manga larga o corta (opcional).</div>
									<div class="itemvest col-xs-12">Pantalones ligeros.</div>
									<div class="itemvest col-xs-12">Colores vivos y tonos claros.</div>
								</div>
							</div>
							<div id="ellosNo" class=" medio col-sm-6 col-xs-12">
								<div class="NO col-xs-12">Hell No!!!</div>
								<div class="vestlista col-xs-12">
									<div class="itemvest col-xs-12">Cholitas.</div>
									<div class="itemvest col-xs-12">Bermudas.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div><!-- atuendo-->

		</div>
	</div>
	<div id="footer" class="invisible">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		</div>
			
	</div>
	
</body>
</html>
